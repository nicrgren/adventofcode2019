use std::{
    fs,
    io::{self, BufRead, BufReader},
    path,
};

pub mod anyhow {
    pub use anyhow::*;
}

pub fn input_file_lines(
    filename: impl AsRef<str>,
) -> Result<impl IntoIterator<Item = String>, io::Error> {
    let log_filename = String::from(filename.as_ref());
    let mut pb = path::PathBuf::new();
    pb.push("inputs");
    pb.push(filename.as_ref());

    let rdr = BufReader::new(fs::File::open(&pb)?);

    Ok(rdr.lines().map(move |res| match res {
        Ok(line) => line,
        Err(err) => {
            panic!(format!(
                "Failed to read line from input file `{}`: {}",
                log_filename, err
            ));
        }
    }))
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
